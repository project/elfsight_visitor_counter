<?php

namespace Drupal\elfsight_visitor_counter\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightVisitorCounterController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/visitor-counter/?utm_source=portals&utm_medium=drupal&utm_campaign=visitor-counter&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
